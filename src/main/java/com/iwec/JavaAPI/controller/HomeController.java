package com.iwec.JavaAPI.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping()
public class HomeController {

	@RequestMapping
	public String home() {

		System.out.println("Welcome home");

		return "Welcome home";

	}

	@RequestMapping(value = "echo/{name}", method = RequestMethod.GET, produces = "application/json")
	public String echo(@PathVariable String name) {

		System.out.println("Welcome to test:" + name);

		return "{\"message\":\"Welcome " + name + " " + "\"}";

	}

}
